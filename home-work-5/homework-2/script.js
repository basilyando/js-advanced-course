window.onload=function(){

var disableBtn = document.getElementsByClassName('disable');

	function handlerDisable(e) {
		let elRemove = e.target.previousElementSibling;
	    elRemove.remove();
	}
	
 	Array.from(disableBtn).forEach(function(element) {
    	element.addEventListener("click", handlerDisable);
	});

};


