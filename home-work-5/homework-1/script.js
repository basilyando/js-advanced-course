window.onload=function(){

var disableBtn = document.getElementById('disable');
var showBtn = document.getElementById('show');

	function handler(e) {
		let text = e.target.parentNode.firstElementChild;
	    text.classList.toggle('visible');
	    console.log(text);
	}

	function handlerDisable() {

	    showBtn.removeEventListener("click", handler);
	    
	}

	showBtn.addEventListener("click", handler);
	disableBtn.addEventListener("click", handlerDisable);
};

			