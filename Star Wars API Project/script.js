window.addEventListener('load', event =>  {
 
 let f = 1;
 let baseUrl = 'https://swapi.co/api/people/';
 let filmsUrl = 'https://swapi.co/api/films/';


 fetch(baseUrl + '?page=' + f, {mode:'cors'}).then(
 function(response) {  
   if (response.status !== 200) {  
    console.log('Response error occured. Status Code: ' + response.status);  
    return;
   }
   response.json().then(function(data) {

    let users = (data.results);
    const resultHTML = document.getElementById("result");
    
    for (let i = 0; i < 10; i++) {

       resultHTML.innerHTML += ("<li>" + users[i].name + "</li><ul class='table'>" + "<li>Gender: " + users[i].gender + "</li><li>BirthYear: " + users[i].birth_year + "</li><li>Movies: " + "</ul>"); 

   }

   
    fetch(filmsUrl, {mode:'cors'}).then(
			function(response) { 

			response.json().then(function(data) {

		    let films = (data.results);
		    console.log(films);
		      })
	})

    })
  }).catch(function(err) {  
   console.log('Fetch Error :-S', err);  
  });
  
document.getElementById('next').addEventListener('click', function(event){
		if (f < 9) {
		   f++;
		    fetch(baseUrl + '?page=' + f, {mode:'cors'}).then(
		    function(response) {
		        response.json().then(function(data) {  
		    let users = (data.results);
		    const resultHTML = document.getElementById("result");
		        resultHTML.innerHTML = '';
		    for (let i = 0; i < users.length; i++) {
	      resultHTML.innerHTML += ("<li>" + users[i].name + "</li><ul class='table'>"  + "<li>Gender: " + users[i].gender + "</li><li>BirthYear: " + users[i].birth_year + "</li><li>Movies: " + "</ul>"); 
		    }

		   })

		  })
		}
 });

 document.getElementById('prev').addEventListener('click', function(event){
   		if (f > 1) {
		    f--;

		    fetch(baseUrl + '?page=' + f, {mode:'cors'}).then(
		    function(response) {
		        response.json().then(function(data) {  
		    let users = (data.results);
		    const resultHTML = document.getElementById("result");
		        resultHTML.innerHTML = '';
			    for (let i = 0; i < 10; i++) {
      resultHTML.innerHTML += ("<li>" + users[i].name + "</li><ul class='table'>"  + "<li>Gender: " + users[i].gender + "</li><li>BirthYear: " + users[i].birth_year + "</li><li>Movies: " + "</ul>"); 

			    }
		   })
		  })
		}
 	});
});