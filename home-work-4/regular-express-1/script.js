window.onload = function(){

		var findIpBtn = document.getElementById('find-ip');
		var findZipBtn = document.getElementById('find-zip');
		var findPriceBtn = document.getElementById('find-price');
		var findDateBtn = document.getElementById('find-date');
		var str = document.getElementById('text').innerText;


		findIpBtn.addEventListener("click", myFunctionIp);
		findZipBtn.addEventListener("click", myFunctionZip);
		findPriceBtn.addEventListener("click", myFunctionPrice);
		findDateBtn.addEventListener("click", myFunctionDate);


		function myFunctionIp(){
			let regexp = /(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/g;
			let array = [...str.matchAll(regexp)];
			for(var i = 0; i < array.length; i++) {
       		 		alert(array[i]);
    			};
		}

		function myFunctionZip() {
		    
		    let regexp = /\b\d{5}\b/g;
		    let array = [...str.matchAll(regexp)];
		   	for(var i = 0; i < array.length; i++) {
       		 		alert(array[i]);
    			};

		};

		function myFunctionPrice() {
		   
		   let regexp = /\$\d+(?:\.\d+)?/g;

		   let array = [...str.matchAll(regexp)];
			for(var i = 0; i < array.length; i++) {
       		 		alert(array[i]);
    			};

		};

		function myFunctionDate() {

			let regexp = /(?:[0-2][0-9]|(?:3)[0-1])(?:\/)(?:(?:(?:0)[0-9])|(?:(?:1)[0-2]))(?:\/)(?:\d{4})/g;
		    let array = [...str.matchAll(regexp)];
			for(var i = 0; i < array.length; i++) {
       		 		alert(array[i]);
    			};

		};
};